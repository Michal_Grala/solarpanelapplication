package com.example.solarpanelapplication

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.*
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style

class MainActivity : AppCompatActivity(), OnMapReadyCallback, OnLocationClickListener, PermissionsListener,
    OnCameraTrackingChangedListener {

    private var mapView: MapView? = null
    private lateinit var permissionsManager: PermissionsManager
    private lateinit var mapboxMap: MapboxMap
    private lateinit var locationComponent: LocationComponent
    private var isTrackingMode: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(
            this,
            "pk.eyJ1IjoibWljaGFsZ3JhbGEiLCJhIjoiY2p5NDd0OXp0MGhhNTNibzM1ZTBxazF5bCJ9.80sT1VN-H93tgjGSeP2iiw"
        )
        setContentView(R.layout.activity_main)

        mapView = findViewById(R.id.mapView)
        mapView?.onCreate(savedInstanceState)
        mapView?.getMapAsync(this)
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        mapboxMap.setStyle(Style.LIGHT, Style.OnStyleLoaded() {

        })
        }

    private fun enableLocationComponent(loadedMapStyle: Style) {
        if(PermissionsManager.areLocationPermissionsGranted(this)) {
            val customLocationComponentOptions = LocationComponentOptions.builder(this)
                .elevation(5f)
                .accuracyAlpha(.6f)
                .accuracyColor(Color.RED)
                .foregroundDrawable(R.drawable.ic_launcher_foreground)
                .build()

            locationComponent = mapboxMap.locationComponent

            val locationComponentActivationOptions = LocationComponentActivationOptions.builder(this, loadedMapStyle)
                .locationComponentOptions(customLocationComponentOptions)
                .build()


            //locationComponent.setLocationComponentEnabled(true);
            locationComponent.activateLocationComponent(locationComponentActivationOptions)

            locationComponent.cameraMode = CameraMode.TRACKING


        }
    }

    override fun onLocationComponentClick() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPermissionResult(granted: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCameraTrackingChanged(currentMode: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCameraTrackingDismissed() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

//    public override fun onStart() {
//        super.onStart()
//        mapView!!.onStart()
//    }
//
//    public override fun onResume() {
//        super.onResume()
//        mapView!!.onResume()
//    }
//
//    public override fun onPause() {
//        super.onPause()
//        mapView!!.onPause()
//    }
//
//    public override fun onStop() {
//        super.onStop()
//        mapView!!.onStop()
//    }
//
//    override fun onLowMemory() {
//        super.onLowMemory()
//        mapView!!.onLowMemory()
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        mapView!!.onDestroy()
//    }
//
//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        mapView!!.onSaveInstanceState(outState)
//    }

}


